import { defineConfig } from 'vite'
const { createVuePlugin } = require('vite-plugin-vue2')

const config = defineConfig({
  resolve: {
    alias: {
      // Tells vite to import the full Vue 2 package including template compiler.
      // Default import unfortunately picks the version without the included template compiler.
      'vue': 'vue/dist/vue.js',

      // Defines where imports starting from @/ prefix are found
      '@/': __dirname + '/src/',
    },
  },

  plugins: [
    // Vue2-compatible plugin for .vue single-file components
    createVuePlugin({})
  ],
})

export default config