import Vue from 'vue'
import './style.scss'
import App from './app.vue'

var app = new Vue({
  el: '#app',
  components: {
    App,
  },
  template: '<App></App>'
})
